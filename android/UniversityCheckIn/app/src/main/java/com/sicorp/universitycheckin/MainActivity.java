package com.sicorp.universitycheckin;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
    // elements
    EditText editTextName;
    EditText editTextSurname;
    EditText editTextMidlename;
    //---------
    protected String Account = "no account";
    protected final String[] faculties = {"КН", "ПММ", "КИУ", "РТ"};
    protected final String[][] departments = {
            {"ПІ", "ІУС", "МСТ", "СТ", "ШІ"},
            {"ВМ", "ЕК", "ІНФ", "ПМ", "СІ"},
            {"АПОТ", "БІТ", "ЕОМ", "Філ"},
            {"ІнМ", "ОРТ", "РЕС", "РТІКС"}
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextName = (EditText)findViewById(R.id.editTextName);
        editTextSurname = (EditText)findViewById(R.id.editTextSurname);
        editTextMidlename = (EditText)findViewById(R.id.editTextMidleName);

        this.Account = getGoogleAccount();
        if(checkGoogleAccount()){
            Intent intent = new Intent(MainActivity.this, UIActivity.class);
            intent.putExtra("account", Account);
            startActivity(intent);
        }

        initSpinners();
    }

    public void buttonConfirm_onClick(View view) {
        if(inputIsValid()){
            String sendingString = editTextName.getText().toString() + "|" +
                    editTextSurname.getText().toString() + "|" +
                    editTextMidlename.getText().toString() + "|" + Account;
            //TODO отправить на сервер
        }
    }

    private boolean inputIsValid() {
        TextView textViewShowError = (TextView) findViewById(R.id.textViewShowError);
        if(editTextName.getText().toString().equals("")){
            textViewShowError.setText(R.string.error_name);
            return false;
        }
        if(editTextSurname.getText().toString().equals("")){
            textViewShowError.setText(R.string.error_surname);
            return false;
        }
        if(editTextMidlename.getText().toString().equals("")){
            textViewShowError.setText(R.string.error_midlename);
            return false;
        }
        if(getGoogleAccount().equals("no account")){
            textViewShowError.setText(R.string.error_account);
            return false;
        }
        return true;
    }

    private boolean checkGoogleAccount() {
        if(!this.Account.equals("no account")) {
            //TODO сверить с БД на сервере
            return true;
        }
        return false;
    }

    private String getGoogleAccount() {
        AccountManager manager = (AccountManager) getSystemService(ACCOUNT_SERVICE);
        android.accounts.Account[] list = manager.getAccounts();
        for(Account account: list) {
            if (account.type.equalsIgnoreCase("com.google")) {
                return account.name;
            }
        }
        return "no account";
    }

    private void initSpinners() {
        Spinner spinnerFac = (Spinner) findViewById(R.id.spinnerFaculty);
        // адаптер
        ArrayAdapter<String> adapterForFacs = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, faculties);
        adapterForFacs.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFac.setAdapter(adapterForFacs);
        // заголовок
        spinnerFac.setPrompt(getString(R.string.faculties));
        // выделяем элемент
        spinnerFac.setSelection(0);
        // устанавливаем обработчик нажатия
        spinnerFac.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Spinner spinnerDep = (Spinner) findViewById(R.id.spinnerDepartment);
                // адаптер
                ArrayAdapter<String> adapterForDeps = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_spinner_item, departments[position]);
                adapterForDeps.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerDep.setAdapter(adapterForDeps);
                // заголовок
                spinnerDep.setPrompt(getString(R.string.departments));
                // выделяем элемент
                spinnerDep.setSelection(0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });
    }
}
