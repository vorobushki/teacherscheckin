# Teachers Check In Project#

Project of students which purpose was to give other students an opportunity to check teachers' presence in the university.

### Team ###

* Vladislav Apukhtin (Servlet API and JSP/JSTL + Tomcat 8 + JDBC + MySQL + Deploy) | Branch **server**
* Budyanskiy Alexey (HTML/CSS/JS + Bootstrap) | Branch **client**
* Iordanov Sergey (Java Android + Google API) | Branch **android**

2015 (C)